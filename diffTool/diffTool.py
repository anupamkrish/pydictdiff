class Diff:

    def __init__(self, p, q, exclude = []):

        self.d1, self.d2 = p, q
        self.exclude = exclude

        self.depth = 0                          # Keep track of depth
        self.pathbuilder = ""                   # Maintain trace of node traversed
        self.diffs = []                         # Store differences in list
        self.process(self.d1, self.d2)          # Driver


    # Calculate diffs between d1, d2
    def process(self, d1, d2):
        for i in d1:
            if i not in self.exclude:
                if self.depth == 0:
                    self.pathbuilder = i
                if isinstance(d1[i], dict):     # Call diff recursively on non-leaf nodes
                    self.depth += 1
                    if self.pathbuilder != i:
                        self.pathbuilder += " -> " + i if self.pathbuilder else i
                    self.process(d1[i], d2[i])
                else:                           # Store diff for leaf node
                    if self.pathbuilder != i:
                        _path = self.pathbuilder + " -> " + i
                    else:
                        _path = i
                    if d1[i] != d2[i]:
                        self.diffs.append({
                            _path: {
                                "[-]": d1[i],
                                "[+]": d2[i]
                            }
                        })
        self.depth -= 1